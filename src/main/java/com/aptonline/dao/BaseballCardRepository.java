package com.aptonline.dao;

import org.springframework.data.repository.CrudRepository;

import com.aptonline.model.BaseballCard;

public interface BaseballCardRepository extends CrudRepository<BaseballCard,Long> {

}
