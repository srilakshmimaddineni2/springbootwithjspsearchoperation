package com.aptonline.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptonline.dao.BaseballCardRepository;
import com.aptonline.model.BaseballCard;

@Service
public class CardServiceImpl implements CardService {

    @Autowired
    BaseballCardRepository cardrepository;

    BaseballCard bbc = new BaseballCard();
    BaseballCard bbc1 = new BaseballCard();
    BaseballCard bbc2 = new BaseballCard();

    public void addCards() {
    	bbc.setName("AA");
    	bbc.setYear(2017);
    	bbc.setRarityLevel("Very Rare");

        cardrepository.save(bbc);

        bbc1.setName("BB");
        bbc1.setYear(2018);
        bbc1.setRarityLevel("Very Rare");

        cardrepository.save(bbc1);

        bbc2.setName("CC");
        bbc2.setYear(1909);
        bbc2.setRarityLevel("Rarest");

        cardrepository.save(bbc2);

        System.out.println("Cards have been added : " + cardrepository.findAll());

    }
}
